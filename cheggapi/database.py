import pymysql

class MySql():
    def __init__(self):
        conn = pymysql.connect(host='localhost', user='root', passwd='1234', database='chegg', charset='utf8')
        self.conn = conn
    
    def saveTopic(self, topic_info):
        cursor = self.conn.cursor()
        try:
            sql = "INSERT INTO topic(topic_Id, url, question) Values(%s, %s, %s)"
            item = (topic_info['topic_Id'], topic_info['url'], topic_info['question'])
            cursor.execute(sql, item)
            self.conn.commit()
        except:
            print(f"{topic_info['id']} 已存在於資料庫")
    
    def saveTopicContent(self, topic_content):
        cursor = self.conn.cursor()
        try:
            sql = "INSERT INTO topic_content(Id, topic_Id, content, type) Values(%s, %s, %s, %s)"
            item = (topic_content['id'], topic_content['topic_Id'], topic_content['content'], topic_content['type'])
            cursor.execute(sql, item)
            self.conn.commit()
        except:
            print(f"{topic_content['id']} 已存在於資料庫")
    
    def saveTopicAnswer(self, topic_content):
        cursor = self.conn.cursor()
        try:
            sql = "INSERT INTO topic_answer(Id, topic_Id, content, type) Values(%s, %s, %s, %s)"
            item = (topic_content['id'], topic_content['topic_Id'], topic_content['content'], topic_content['type'])
            cursor.execute(sql, item)
            self.conn.commit()
        except:
            print(f"{topic_content['id']} 已存在於資料庫")

    def getTopicsInfo(self):
        cursor = self.conn.cursor(pymysql.cursors.DictCursor)
        sql = "SELECT * FROM topic"
        cursor.execute(sql)
        data = cursor.fetchall()
        return data

    def getTopicsInfoByTopicIdsAndStartAndEnd(self, topic_Id_arr, start, end):
        cursor = self.conn.cursor(pymysql.cursors.DictCursor)
        sql = "SELECT topic_Id, question FROM topic WHERE "
        for topic_Id in topic_Id_arr:
            sql += f"topic_Id = {topic_Id} or "
        sql = sql[:-4] + f" LIMIT {start}, {end}"
        cursor.execute(sql)
        data = cursor.fetchall()
        return data
    
    def getTopicsCountByTopicIds(self, topic_Id_arr):
        cursor = self.conn.cursor(pymysql.cursors.DictCursor)
        sql = "SELECT COUNT(*) FROM topic WHERE "
        for topic_Id in topic_Id_arr:
            sql += f"topic_Id = {topic_Id} or "
        sql = sql[:-4]
        cursor.execute(sql)
        count = cursor.fetchone()['COUNT(*)']
        return count

    def getTopicContentByTopicId(self, topic_Id):
        cursor = self.conn.cursor(pymysql.cursors.DictCursor)
        sql = f"SELECT Id, content, type FROM topic_content WHERE topic_Id = {topic_Id}"
        cursor.execute(sql)
        data = cursor.fetchall()
        return data

    def getTopicAnswerByTopicId(self, topic_Id):
        cursor = self.conn.cursor(pymysql.cursors.DictCursor)
        sql = f"SELECT Id, content, type FROM topic_answer WHERE topic_Id = {topic_Id}"
        cursor.execute(sql)
        data = cursor.fetchall()
        return data

    def close(self):
        self.conn.close()
