from flask import Flask, request
from flask_cors import CORS
import json
from database import MySql
from information_search import InvertedIndex, CosineSimilarity
from service import questionProcess

def buildInvertedIndexFile():
    mysql = MySql()
    data = mysql.getTopicsInfo()
    mysql.close()
    inverted = InvertedIndex(data)
    inverted.createInvertedIndex()

app = Flask(__name__, static_url_path='/')
CORS(app)
buildInvertedIndexFile()
cs = CosineSimilarity('inverted.txt')
server_route = "http://localhost:5000"

# 取得相似度的搜尋結果(由高到低)
@app.route("/search", methods = ['GET'])
def search():
    try:
        query = request.args['query']
        start = int(request.args['start']) - 1
        num = int(request.args['num'])
        score = cs.calculateCosineSimilarity(query)
        mysql = MySql()
        topics = mysql.getTopicsInfoByTopicIdsAndStartAndEnd(score.keys(), start, num)
        # 將要回傳的題目新增分數以及切割內容
        for topic in topics:
            topic['score'] = score[str(topic['topic_Id'])]
            topic['question'] = questionProcess(query, topic['question'])
        # 對題目進行排序，分數高的先排
        topics = sorted(topics, key = lambda topic: topic['score'], reverse = True)
        count = mysql.getTopicsCountByTopicIds(score.keys())
        mysql.close()
        result = {'data': topics, 'count': count, 'status': True}
    except:
        result = {'message': '查無資料', 'status': False}
    result = json.dumps(result)
    return result

# 取得內容資訊
@app.route("/topicInfo/<int:topic_Id>", methods = ["GET"])
def topicInfo(topic_Id):
    try:
        mysql = MySql()
        data = mysql.getTopicContentByTopicId(topic_Id)
        for item in data:
            if item['type'] == 'picture':
                item['content'] = f"{server_route}/images/topic_pictures/{item['content']}"
        result = {'data': data, 'status': True}
    except:
        result = {'message': '發生錯誤', 'status': False}
    result = json.dumps(result)
    return result

# 取得答案資訊
@app.route("/answerInfo/<int:topic_Id>", methods = ["GET"])
def answerInfo(topic_Id):
    try:
        mysql = MySql()
        data = mysql.getTopicAnswerByTopicId(topic_Id)
        for item in data:
            if item['type'] == 'picture':
                item['content'] = f"{server_route}/images/answer_pictures/{item['content']}"
        result = {'data': data, 'status': True}
    except:
        result = {'message': '發生錯誤', 'status': False}
    result = json.dumps(result)
    return result

if __name__ == '__main__':
    app.run()
