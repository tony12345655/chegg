
def questionProcess(query, question):
    result = []
    question_split  = question.split()
    query_list = query.lower().split()
    for i in range(0, len(question_split)):
        text = question_split[i].lower()
        if text in query_list:
            keyWord = True
        else:
            keyWord = False
        result.append({'Id': i + 1, 'text': question_split[i], 'keyWord': keyWord})
    return result
