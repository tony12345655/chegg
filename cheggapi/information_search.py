import nltk
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer
import string
import math
from tqdm import tqdm
from sklearn.metrics.pairwise import cosine_similarity

def downloadPackage():
    nltk.download('wordnet')
    nltk.download('omw-1.4')
    nltk.download('stopwords')

def textPreprocess(text):
    text = text.lower()
    not_need_char_arr = ['\r', '\n', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '©', '«', '»', '•','—']
    for ch in not_need_char_arr:
        text = text.replace(ch, ' ')
    for pun in string.punctuation:
        text = text.replace(pun, ' ')
    return text

def textLemmatizer(text):
    lm = WordNetLemmatizer()
    text = lm.lemmatize(text)
    return text

def judgeStopWords(word):
    if word in stopwords.words('english'):
        return True
    else:
        return False

class InvertedIndex():
    def __init__(self, data):
        downloadPackage()
        self.data = data

    @staticmethod
    def __splitAndProcessDoc(data):
        doc_arr = []
        for doc in tqdm(data):
            question = textPreprocess(doc['question'])
            doc_arr.append({'topic_Id': doc['topic_Id'], 'question': question.split()})
        return doc_arr

    @staticmethod
    def __findUniqueWord(split_doc_arr):
        unique_word_dict = {}
        for doc in split_doc_arr:
            word_arr = doc['question']
            word_dict = {}
            for word_index in range(0, len(word_arr)):
                word = textLemmatizer(word_arr[word_index])
                if word not in word_dict.keys():
                    word_dict[word] = {"count": 1, "frequency": f"<{word_index}"}
                else:
                    word_dict[word]["count"] += 1
                    word_dict[word]["frequency"] += f", {word_index}"
            for (key, value) in word_dict.items():
                if key not in unique_word_dict.keys():
                    unique_word_dict[key] = {"count":  value['count'], "doc": f"   <{doc['topic_Id']},{value['count']}:{value['frequency']}>;\n"}
                else:
                    unique_word_dict[key]["count"] += value['count']
                    unique_word_dict[key]["doc"] += f"    {doc['topic_Id']},{value['count']}:{value['frequency']}>;\n"
        return unique_word_dict

    @staticmethod
    def __printWord(unique_word_dict):
        file_name = 'inverted.txt'
        f = open(file_name, 'w', encoding='utf-8')
        for (key, value) in tqdm(unique_word_dict.items()):
            if judgeStopWords(key) is False:
                write_text = f"{key},{value['count']}:\n{value['doc']}"[:-1] + ">\n\n"
                f.write(write_text)
        f.close()
        # 刪除最後兩個跳行
        fin  = open(file_name, 'r', encoding='utf-8')
        fin_text_arr = fin.readlines()
        fin.close()
        fout = open(file_name, 'w', encoding='utf-8')
        new_file = ''.join(fin_text_arr[:-1])
        new_file = new_file[:-1]
        fout.write(new_file)
        fout.close()

    def createInvertedIndex(self):
        split_doc_arr = self.__splitAndProcessDoc(self.data)
        unique_word_dict = self.__findUniqueWord(split_doc_arr)
        self.__printWord(unique_word_dict)
        print("InvertedIndex已建立完成")

class CosineSimilarity():
    def __init__(self, path):
        downloadPackage()
        word_dic, topic_Id_arr = self.__createInvertedIndexDictionary(path)
        self.word_dic = word_dic
        self.topic_Id_arr = topic_Id_arr

    @staticmethod
    def __judgeAllWordInDoc(self, terms_arr, doc_number):
        exist = 0
        for word in terms_arr:
            if word in self.word_dic.keys() and doc_number in self.word_dic[word].keys():
                exist += 1
        if exist == len(terms_arr):
            return True
        else:
            return False

    @staticmethod
    def __queryPreprocess(text):
        text = textPreprocess(text)
        query = text.split()
        query_dict = {}
        for word in query:
            lm_word = textLemmatizer(word)
            if lm_word not in query_dict:
                query_dict[lm_word] = 1
            else:
                query_dict[lm_word] += 1
        return query_dict

    @staticmethod
    def __createInvertedIndexDictionary(path):
        print("開始建立InvertedIndex字典")
        data = {}
        topic_Id_arr = set()
        with open(path, 'r', encoding='utf-8') as f:
            word_info_arr = f.read().split('\n\n')
            for word_info in tqdm(word_info_arr):
                word_arr = word_info.split('\n')
                word = word_arr[0].split(',')[0]
                doc_index_arr = word_arr[1:]
                doc_word_count = {}
                for doc in doc_index_arr:
                    doc_split = doc.split(',')
                    doc = doc_split[0].replace('<', '').replace(' ','')
                    doc_word_count[doc] = doc_split[1].split(':')[0]
                    if doc not in topic_Id_arr:
                        topic_Id_arr.add(doc)
                data[word]= doc_word_count
        print("建立完成")
        return data, topic_Id_arr

    def __createDocumentsTFIDF(self, terms_arr, doc_number):
        tfidf_list = []
        for word in terms_arr:
            if judgeStopWords(word) is False:
                if word in self.word_dic.keys():
                    if doc_number in self.word_dic[word].keys():
                        tf = 1 + math.log10(int(self.word_dic[word][doc_number]))
                    else:
                        tf = 0
                    idf = math.log10(len(self.topic_Id_arr) / len(self.word_dic[word]))
                    w = tf * idf
                else:
                    w = 0
                tfidf_list.append(w)
        return tfidf_list

    def __createNormalModeDocumentsTFIDFList(self, terms_arr, topic_Id_arr):
        print("開始建立文章TFIDF串列")
        data = []
        for doc_number in tqdm(topic_Id_arr):
            tfidf_list = self.__createDocumentsTFIDF(terms_arr, str(doc_number))
            data.append({'topic_Id': doc_number, "tfidf_list": tfidf_list})
        print("建立完成")
        return data

    def __createAndModeDocumentsTFIDFList(self, terms_arr, topic_Id_arr):
        print("開始建立文章TFIDF串列")
        data = []
        for doc_number in tqdm(topic_Id_arr):
            if self.__judgeAllWordInDoc(terms_arr, str(doc_number)):
                tfidf_list = self.__createDocumentsTFIDF(terms_arr, str(doc_number))
            else:
                tfidf_list = [0] * len(terms_arr)
            data.append({'topic_Id': doc_number, "tfidf_list": tfidf_list})
        print("建立完成")
        return data

    def __createOrModeDocumentsTFIDFList(self, terms_arr, topic_Id_arr):
        print("開始建立文章TFIDF串列")
        data = []
        for doc_number in tqdm(topic_Id_arr):
            tfidf_list = []
            for terms in terms_arr:
                if self.__judgeAllWordInDoc(terms, str(doc_number)):
                    tfidf_list += self.__createDocumentsTFIDF(terms , str(doc_number))
                else:
                    tfidf_list += [0] * len(terms)
            data.append({'topic_Id': doc_number, "tfidf_list": tfidf_list})
        print("建立完成")
        return data

    def __createQueryTFIDFList(self, query_dict):
        print("開始建立queryTFIDF串列")
        data = []
        for (word, count) in tqdm(query_dict.items()):
            if judgeStopWords(word) is False:
                if word in self.word_dic.keys():
                    tf = 1 + math.log10(count)
                    idf = math.log10(len(self.topic_Id_arr)/ len(self.word_dic[word]))
                    w = tf * idf
                else:
                    w = 0
                data.append(w)
        print("建立完成")
        return data

    def __cosineSimilarityScore(self, query_dict, terms_arr, mode):
        if mode == 'AND':
            doc_tfidf_arr = self.__createAndModeDocumentsTFIDFList(terms_arr, self.topic_Id_arr)
        elif mode == 'OR':
            doc_tfidf_arr = self.__createOrModeDocumentsTFIDFList(terms_arr, self.topic_Id_arr)
        else:
            doc_tfidf_arr = self.__createNormalModeDocumentsTFIDFList(terms_arr, self.topic_Id_arr)
        query_tfidf = self.__createQueryTFIDFList(query_dict)
        cosine_similarity_score = {}
        print("開始比較CosineSimilarity")
        for doc in tqdm(doc_tfidf_arr):
            cosine_similarity_score[doc['topic_Id']] = round(cosine_similarity([query_tfidf], [doc['tfidf_list']])[0, 0], 3)
        data = {}
        while (len(cosine_similarity_score) != 0):
            max_key = max(cosine_similarity_score, key = cosine_similarity_score.get)
            score = cosine_similarity_score.pop(max_key)
            if score > 0: # 大於0才要
                data[max_key] = score
        print("比較完成")
        return data

    def __orTermsPreprocess(self, text):
        terms_arr = text.split('OR')
        for terms_index in range(0, len(terms_arr)):
            terms_arr[terms_index] = self.__queryPreprocess(terms_arr[terms_index]).keys()
        return terms_arr

    def __andTermsPreprocess(self, text):
        text = text.replace('AND', ' ')
        terms_arr = self.__queryPreprocess(text).keys()
        return terms_arr

    def __normalTermsPreprocess(self, text):
        terms_arr = self.__queryPreprocess(text).keys()
        return terms_arr

    def calculateCosineSimilarity(self, text):
        mode = 'Normal'
        if 'AND' in text:
            mode = 'AND'
            terms_arr = self.__andTermsPreprocess(text)
        elif 'OR' in text:
            mode = 'OR'
            terms_arr = self.__orTermsPreprocess(text)
        else:
            terms_arr = self.__normalTermsPreprocess(text)
        if len(terms_arr) == 0:
            return "請勿輸入空白查詢"
        else:
            query_dict = self.__queryPreprocess(text)
            result = self.__cosineSimilarityScore(query_dict, terms_arr, mode)
            return result