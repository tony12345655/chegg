import { createRouter, createWebHashHistory, RouteRecordRaw } from 'vue-router'
import TopicsView from '../views/TopicsView.vue'
import Topic from '../views/Topics/Id.vue'
const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'Topics',
    component: TopicsView
  },
  {
    path: '/topic/:_Id',
    name: 'Topic',
    component: Topic
  }
]

const router = createRouter({
  history: createWebHashHistory(process.env.BASE_URL),
  routes
})

export default router
