import { createStore } from 'vuex'
import axios from 'axios'
export default createStore({
  state: {
    query: {query: "", start: 1, num: 5, loading: false},
    topics: {data: [], now_page: 1, total: 0}
  },
  getters: {
    query(state){
      return state.query;
    },
    topics(state){
      return state.topics;
    }
  },
  mutations: {
    getSearchContent(state, payload){
      if (payload.status){
        state.topics.data = payload.data
        state.topics.total = Math.ceil(payload.count / 5);
        if (state.topics.now_page === 1)
            state.topics.now_page = 1;
      }
      state.query.loading = false;
    },
    changeQuery(state, payload){
      if (payload.query != null)
        state.query.query = payload.query
      if (payload.start != null)
        state.query.start = payload.start
      if (payload.num != null)
        state.query.num = payload.num
      if (payload.loading != null)
        state.query.loading = payload.loading
    },
    changeTopic(state, payload){
      if (payload.data != null)
        state.topics.data = payload.data
      if (payload.now_page != null)
        state.topics.now_page = payload.now_page
      if (payload.total != null)
        state.topics.total = payload.total
    }
  },
  actions: {
    getSearchContent({commit}, {query}){
      axios.get("http://localhost:5000/search", {params: query})
      .then((res) => {
        commit('getSearchContent', res.data)
      })
    },
    changeQuery({commit}, {query=null, start=null, num=null, loading=null}){
      commit('changeQuery', {query, start, num, loading})
    },
    changeTopic({commit}, {data=null, now_page=null, total=null}){
      commit('changeTopic', {data, now_page, total})
    },
  }
})
