import pymysql

class MySql():
    def __init__(self):
        conn = pymysql.connect(host='localhost', user='root', passwd='1234', database='chegg', charset='utf8')
        self.conn = conn
    
    def SaveTopic(self, topic_info):
        cursor = self.conn.cursor()
        try:
            sql = "INSERT INTO topic(topic_Id, url, question) Values(%s, %s, %s)"
            item = (topic_info['topic_Id'], topic_info['url'], topic_info['question'])
            cursor.execute(sql, item)
            self.conn.commit()
        except:
            print(f"{topic_info['id']} 已存在於資料庫")
    
    def SaveTopicContent(self, topic_content):
        cursor = self.conn.cursor()
        try:
            sql = "INSERT INTO topic_content(Id, topic_Id, content, type) Values(%s, %s, %s, %s)"
            item = (topic_content['id'], topic_content['topic_Id'], topic_content['content'], topic_content['type'])
            cursor.execute(sql, item)
            self.conn.commit()
        except:
            print(f"{topic_content['id']} 已存在於資料庫")
    
    def SaveTopicAnswer(self, topic_content):
        cursor = self.conn.cursor()
        try:
            sql = "INSERT INTO topic_answer(Id, topic_Id, content, type) Values(%s, %s, %s, %s)"
            item = (topic_content['id'], topic_content['topic_Id'], topic_content['content'], topic_content['type'])
            cursor.execute(sql, item)
            self.conn.commit()
        except:
            print(f"{topic_content['id']} 已存在於資料庫")

    def GetTopicsInfo(self):
        cursor = self.conn.cursor(pymysql.cursors.DictCursor)
        sql = "SELECT * FROM topic"
        cursor.execute(sql)
        data = cursor.fetchall()
        return data
        
    def DBClose(self):
        self.conn.close()