import requests
from bs4 import BeautifulSoup
from database import MySql
import json
import time

def getTopicsInfo(page, query):
    header = {"cookie": "V=b2140bf89fc7eabc45c7ba72c972b7c9634a3025a45a1c.51511721; _omappvp=UFRKvcf3aSH3paUwrlWwnEYAMNkNJTD0ldhenocuyNGLdwXaI6g1XHhfD122ajNf2uZ2xZKL4xXeUw21RL0R7lCpl4G3loN5; _pxvid=c87e8b83-4c3d-11ed-8278-55796c79786e; _ga=GA1.2.26429051.1665806374; _gcl_au=1.1.724149711.1665806374; _cs_c=0; optimizelyEndUserId=oeu1665806518098r0.32436547692540074; sa-user-id=s%253A0-8566b624-fc5a-41e9-5f1c-ca2ef6d51137.%252B4EW5Qab1bARQ8A1rVHjWNCTx0%252FAwRPhdpd02L7WcBY; sa-user-id-v2=s%253A0-8566b624-fc5a-41e9-5f1c-ca2ef6d51137%2524ip%2524140.124.183.33.2e8B89fhsr3Iz2CMZOxbvJEkhzF4yUlYp5VDaBp9GTU; _tt_enable_cookie=1; _ttp=9489700a-f4c3-4b14-9b92-357cc4d39e5b; mdLogger=false; kampyle_userid=3c27-f5bc-4033-ea38-b916-b461-243a-3619; kampyleUserSession=1667637747573; kampyleUserSessionsCount=3; kampyleSessionPageCounter=1; chegg_web_cbr_interstitial=b99; C=0; O=0; forterToken=3436ffb3f0624a7381d6de3b29b6bb4a_1668430001371__UDF43_13ck; refresh_token=ext.a0.t00.v1.McLryPBMBgKhb57Fzw6YbjVI26ZDlqjd6YhCr55jhMO4u9eiFFLE7_9V00XjvZ4vdJ3eevXAu8mxKA24bqP2fnM; id_token=eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InRvbnkxMjM0NTY1NTVAZ21haWwuY29tIiwiaXNzIjoiaHViLmNoZWdnLmNvbSIsInN1YiI6IjhkMDkyMWJmLTUxMzItNDIyYS1hYmJhLTBkMTBhYWFlNDJiNiIsImF1ZCI6IkNIR0ciLCJpYXQiOjE2Njg0MzAwMzMsImV4cCI6MTY4Mzk4MjAzMywicmVwYWNrZXJfaWQiOiJhcHcifQ.CB4IGkHKnp2sENrcWU5eqRDwDYuICxPi1nIIa8YH643r1rErYCU0WCmeM694XiRRkiE8Ye8z4KFkYYW-VURkZ1M1jbdQ2pU9QD_sn4COIhoKFAq_LQlAK_2AxJpy7KAwsdjElZ5lGCtFRaX2_i-xeuj3eAALMXKbsTxCl78n58zvkYc5eyTUjRrPuXm9l2QwjQo9BI-5EHQ39YBwL5weSG-O1ItaT4zRwe2Nc0vAru640ms7_dvIxflb6kMG6qvIpWbuoJACn0t0JO0XYWBGtzS25_zL_iK9g9lxqSIa52Rln5QFVg6GUpudyAacppSiYa-2pQh80-qImDDLa_Tnfw; exp=C026A%7CA127D; expkey=649839316CBE9221315720CCA48652F1; U=5ffced0af7b4881fb277fcd00d6d8d12; gid=1; gidr=MA; _pbjs_userid_consent_data=3524755945110770; _pubcid=61ca9ab2-d50d-420e-937c-823df0cb5d87; _iidt=h3i57FW+hmkMVxDw4TOcBTQpGZifqYpC/vRas6NZU1vQkgW0WzA/34TtmRxdZev3ubc/vbnZucup+aR9ZSLFcCdo7g==; _vid_t=/tV/AMT6agl/bWUp7SGqGvuNVrzGRO4vetYqD8Hs0NGHyKpZHY386uRmbBQibdijuUf/k2JKyykE3iAezT/Gmq23XQ==; DFID=web|rGEWTz86QqtJRvvkJ7Cu; _lr_env_src_ats=false; connectid=%7B%22vmuid%22%3A%22p_caKBDNudkBYgqoalOxr2AjILUBK9FWYpA5nbwhHz2Bu2hxBvFhow_ncE4wJwjncKy574d2dWL5x-llGtUujA%22%2C%22connectid%22%3A%22p_caKBDNudkBYgqoalOxr2AjILUBK9FWYpA5nbwhHz2Bu2hxBvFhow_ncE4wJwjncKy574d2dWL5x-llGtUujA%22%2C%22ttl%22%3A24%7D; pbjs-unifiedid=%7B%22TDID%22%3A%22783b112b-332d-44a0-b75e-eebd198818fa%22%2C%22TDID_LOOKUP%22%3A%22TRUE%22%2C%22TDID_CREATED_AT%22%3A%222022-10-14T12%3A47%3A26%22%7D; usprivacy=1YYY; OneTrustWPCCPAGoogleOptOut=true; _scid=39766ff4-0049-4d46-ad16-214d41c20b6f; _rdt_uuid=1668510439777.a116d4a7-492a-44d9-99b4-bcf415529a5c; _gid=GA1.2.479530940.1668678285; _lr_geo_location=TW; _lr_sampling_rate=100; ln_or=d; CSID=1668761240621; sbm_a_b_test=1-control; ab.storage.deviceId.b283d3f6-78a7-451c-8b93-d98cdb32f9f1=%7B%22g%22%3A%221e576bac-7999-e856-9e76-3ff3b6ce38a2%22%2C%22c%22%3A1667637761399%2C%22l%22%3A1668761242617%7D; ab.storage.userId.b283d3f6-78a7-451c-8b93-d98cdb32f9f1=%7B%22g%22%3A%228d0921bf-5132-422a-abba-0d10aaae42b6%22%2C%22c%22%3A1668430036867%2C%22l%22%3A1668761242618%7D; schoolapi=fa8dbe92-1082-4d1e-b886-a6134b590efb|0.666666667; _lr_retry_request=true; _awl=2.1668761349.0.5-65aa31fcff8638a2f9281d863c2ef5db-6763652d617369612d6561737431-1; pxcts=54c51c1b-671e-11ed-a158-437045525441; _pxff_rf=1; _pxff_fp=1; country_code=TW; CVID=a426103c-cabe-4eb8-b8ce-389e8b71d311; opt-user-profile=b2140bf89fc7eabc45c7ba72c972b7c9634a3025a45a1c.51511721%252C22313072025%253A22408600008; _sdsat_authState=Hard%20Logged%20In; _sdsat_cheggUserUUID=8d0921bf-5132-422a-abba-0d10aaae42b6; local_fallback_mcid=73755696038875434481042411381756477644; s_ecid=MCMID|73755696038875434481042411381756477644; mcid=73755696038875434481042411381756477644; IR_gbd=chegg.com; _gat=1; _cs_cvars=%7B%221%22%3A%5B%22Page%20Name%22%2C%22search%20study%22%5D%7D; _px3=498134ed8f914ddc2eaa0f937bafe9fb7201b38e6fd7522887bd45b03052d392:RSt2Nbs3hgzWjvFNMU247ENd9PeGcKXqxHWDBYQD7zApGfb7ESqzh0T5/o7bDh87DEthV1a3ZIwQaObIqMe93w==:1000:i2/eBAmaFfRxENWvS3xgkRc+93ncrd1aH3wSZjBsOqPofW7G1lyVzuCTlUomzd7KYqLmoSTWKsyaALgKsVITn2LliNc+1pqa0kvoNxoXWH59APedTqEQggQURiyKCG2di4mkn91yTdwfxmJhMVdv/EGn6SIKttv6T21HFydQJxJcyM2hqYIq7dNsUs1XI4qVe2X+d5z9emsFirA+05KNnw==; _px=RSt2Nbs3hgzWjvFNMU247ENd9PeGcKXqxHWDBYQD7zApGfb7ESqzh0T5/o7bDh87DEthV1a3ZIwQaObIqMe93w==:1000:bp3enNG/8sdL+75vozSSUTtQ13rQ+qQ2andMyur6ACyNknTuMej8mu1dD7u6Y5fElswzbEwENf46AeDe8K54k66UdowIAH2s6csfP/zee0atS9RhfZtZ85fP7SIxHwCYetJ0vmymvytxYUS3bfy3YuxsKEB709Vu/wRtR3YAEDJk34LWtM9PI1+Xpi4ya2vlGdStcqAo3C2F1BeSnqelVJmkmBOY7DU/idJl6LgDh3/GIZkgY1Fe0ZU3UxVkl5o6GZIUxGGgaSQFQ/GVM85hTg==; user_geo_location=%7B%22country_iso_code%22%3A%22TW%22%2C%22country_name%22%3A%22Taiwan%22%2C%22region%22%3A%22TPE%22%2C%22region_full%22%3A%22Taipei+City%22%2C%22city_name%22%3A%22Taipei%22%2C%22locale%22%3A%7B%22localeCode%22%3A%5B%22zh-TW%22%5D%7D%7D; OptanonConsent=isIABGlobal=false&datestamp=Fri+Nov+18+2022+16%3A52%3A57+GMT%2B0800+(%E5%8F%B0%E5%8C%97%E6%A8%99%E6%BA%96%E6%99%82%E9%96%93)&version=6.39.0&hosts=&consentId=13e0e439-c784-4f89-8593-99fc88284b53&interactionCount=1&landingPath=NotLandingPage&groups=snc%3A1%2Cfnc%3A1%2Cprf%3A1%2Ctrg%3A1&AwaitingReconsent=false&isGpcEnabled=0; ab.storage.sessionId.b283d3f6-78a7-451c-8b93-d98cdb32f9f1=%7B%22g%22%3A%229232db61-fc05-5d16-3a24-b4a8c8c79e1d%22%2C%22e%22%3A1668763377151%2C%22c%22%3A1668761242616%2C%22l%22%3A1668761577151%7D; PHPSESSID=43iln47uup6fh8l8nu5bi3ekjb; CSessionID=c8de0d6e-489e-46a7-8e45-f8f17ddf8010; SU=pBMHzeXV3MBCvmaPih6bxooZDTYXqa4jtku0PYncK9kUVFl3tW3wh1RwJp-KJBTXtbCwjNbBakdF0L_CVFsYWzZWenG4mHxYW--veFePYj6OuHjydT5GG2T5Ofm0_79U; _tq_id.TV-8145726354-1.ad8a=5fa1ee9f11c0d327.1665806519.0.1668761579..; IR_14422=1668761578854%7C0%7C1668761578854%7C%7C; _uetsid=77c362c0665c11ed95587f56a1ac49a2; _uetvid=c78e58404c3d11ed9830edb1cd2755c1; _cs_id=5f374409-ef25-a45f-cd03-31651d5749f0.1665806519.16.1668761580.1668761244.1.1699970519655; _cs_s=6.0.0.1668763380672", "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36", "authorization": "Basic TnNZS3dJMGxMdVhBQWQwenFTMHFlak5UVXAwb1l1WDY6R09JZVdFRnVvNndRRFZ4Ug==", "apollographql-client-name": "chegg-web"}
    payload = {"operationName":"getSearchStudyResults","variables":{"query":"","profile":"bigegg-srp","page":1,"contentType":"study","contentTypesLimit":[{"contentType":"study","limit":1000}],"showSpellcheck":True,"showUversity":True,"showSqna":True,"useEnhancedIntentClassifier":False,"seasonOverride":"","experiments":""},"query":"query getSearchStudyResults($profile: String, $query: String, $page: Int, $experiments: String, $showSpellcheck: Boolean, $showUversity: Boolean, $showSqna: Boolean, $useEnhancedIntentClassifier: Boolean, $seasonOverride: String, $contentType: String, $courseId: String, $topicId: String, $skipIntent: Boolean, $contentTypesLimit: [ContentTypeLimit]) {\n  search(\n    profile: $profile\n    query: $query\n    page: $page\n    experiments: $experiments\n    showSpellcheck: $showSpellcheck\n    showUversity: $showUversity\n    showSqna: $showSqna\n    useEnhancedIntentClassifier: $useEnhancedIntentClassifier\n    seasonOverride: $seasonOverride\n    contentType: $contentType\n    courseId: $courseId\n    topicId: $topicId\n    skipIntent: $skipIntent\n    contentTypesLimit: $contentTypesLimit\n  ) {\n    order\n    ...IntentsField\n    ...StudyField\n    ...SpellcheckField\n    __typename\n  }\n}\n\nfragment SearchResponseContentFields on SearchResponseContent {\n  numFound\n  start\n  limit\n  __typename\n}\n\nfragment IntentsField on SearchResult {\n  intents {\n    ...SearchIntentFields\n    __typename\n  }\n  orderedIntents {\n    ...SearchIntentFields\n    __typename\n  }\n  __typename\n}\n\nfragment SearchIntentFields on SearchIntent {\n  confidence\n  name\n  sourceReference\n  __typename\n}\n\nfragment StudyField on SearchResult {\n  study {\n    responseContent {\n      docs {\n        __typename\n        ... on SearchQna {\n          id\n          uuid\n          title\n          source\n          questionHighlight\n          question\n          isSqna\n          url\n          answerDisplay\n          score\n          __typename\n        }\n        ... on SearchTbsProblem {\n          id\n          source\n          bookEditionNumber\n          title\n          bookTitle\n          chapterName\n          answerDisplay\n          problemName\n          ean\n          url\n          question\n          questionHighlight\n          isSqna\n          solutionId\n          score\n          __typename\n        }\n      }\n      ...SearchResponseContentFields\n      __typename\n    }\n    __typename\n  }\n  __typename\n}\n\nfragment SpellcheckField on SearchResult {\n  spellcheck {\n    responseContent {\n      docs {\n        __typename\n        ... on SearchSpellcheck {\n          correctedQuery\n          confidence\n          __typename\n        }\n      }\n      ...SearchResponseContentFields\n      __typename\n    }\n    __typename\n  }\n  __typename\n}\n"}
    payload['variables']['query'] = query
    payload['variables']['page'] = page
    res = requests.post("https://gateway.chegg.com/one-graph/graphql", headers = header, json = payload)
    docs = res.json()["data"]["search"]["study"]["responseContent"]["docs"]
    topics = []
    for doc in docs:
        topic = {'url': "https://www.chegg.com" + doc["url"], 'topic_Id': int(doc["id"].replace('q', '').replace('p', '')), 'question': doc['question']}
        topics.append(topic)
    return topics

def getTopics(topic):
    header = {"cookie": "V=0ace4c88b9a1fc011c937fc41a8668236395d00bdbbe31.36945934; CSessionID=85620575-ae43-4eca-93fb-c35aac69013e; userData=%7B%22authStatus%22%3A%22Logged%20Out%22%2C%22attributes%22%3A%7B%22uvn%22%3A%220ace4c88b9a1fc011c937fc41a8668236395d00bdbbe31.36945934%22%7D%7D; CVID=dc23e185-33d5-4e62-bef4-6dce4c269ff2; CSID=1670762507181; local_fallback_mcid=13875539361641493219129841766138759370; s_ecid=MCMID|13875539361641493219129841766138759370; mcid=13875539361641493219129841766138759370; pxcts=2db53691-7951-11ed-b0fc-706c4756666e; _pxvid=2db5291a-7951-11ed-b0fc-706c4756666e; _sdsat_authState=Logged%20Out; _cs_c=0; _ga=GA1.2.1664085121.1670762509; _gid=GA1.2.1053787669.1670762509; _gcl_au=1.1.1767081041.1670762509; _fbp=fb.1.1670762509257.1001393277; IR_gbd=chegg.com; _tt_enable_cookie=1; _ttp=889db8f6-4596-484b-be2e-e35c2958c948; schoolapi=fa8dbe92-1082-4d1e-b886-a6134b590efb|0.666666667; country_code=TW; usprivacy=1YYY; sbm_a_b_test=1-control; __gads=ID=1a3ceddc78852808:T=1670762516:S=ALNI_MaykYyfhCT5ZBdDJgTmg8AuQ0ZS7A; __gpi=UID=00000b8e319bb091:T=1670762516:RT=1670762516:S=ALNI_MYVmg4NKpLWQec2gaFxOjUI2i-pFw; _schn=_gqd9vy; _scid=f7ee86b9-fadd-46bd-a3a6-7bdb93a4b456; ln_or=d; _sctr=1|1670688000000; intlPaQExitIntentModal=hide; optimizelyEndUserId=oeu1670762522097r0.6358579911699487; OneTrustWPCCPAGoogleOptOut=true; PHPSESSID=03ade494cd99cf17ef3029b410441475; user_geo_location=%7B%22country_iso_code%22%3A%22TW%22%2C%22country_name%22%3A%22Taiwan%22%2C%22region%22%3A%22TPE%22%2C%22region_full%22%3A%22Taipei+City%22%2C%22city_name%22%3A%22Taipei%22%2C%22locale%22%3A%7B%22localeCode%22%3A%5B%22zh-TW%22%5D%7D%7D; exp=C026A; expkey=ECFBA3810E1B8330C521A92E0BAE224D; mdLogger=false; kampyle_userid=984b-c2bb-f173-e698-b219-f7a5-57a7-2f49; _cs_cvars=%7B%221%22%3A%5B%22Page%20Name%22%2C%22question%20page%22%5D%2C%222%22%3A%5B%22Experience%22%2C%22desktop%22%5D%2C%223%22%3A%5B%22Page%20Type%22%2C%22landingPage%22%5D%2C%224%22%3A%5B%22Auth%20Status%22%2C%22Logged%20Out%22%5D%7D; _pxff_rf=1; _gat=1; hwh_order_ref=/homework-help/questions-and-answers/bigo-transpose-input-one-2d-list-return-transpose-input-def-transpose-m-q44912701; OptanonConsent=isGpcEnabled=0&datestamp=Sun+Dec+11+2022+20%3A45%3A43+GMT%2B0800+(%E5%8F%B0%E5%8C%97%E6%A8%99%E6%BA%96%E6%99%82%E9%96%93)&version=6.39.0&isIABGlobal=false&hosts=&consentId=36de6c9f-22e3-4906-a039-a912d5f4e63e&interactionCount=1&landingPath=NotLandingPage&groups=fnc%3A1%2Csnc%3A1%2Ctrg%3A1%2Cprf%3A1&AwaitingReconsent=false; _tq_id.TV-8145726354-1.ad8a=512e016ae8348177.1670762510.0.1670762744..; IR_14422=1670762744022%7C0%7C1670762744022%7C%7C; _uetsid=2e0ca0b0795111edb5d065c5dea245cd; _uetvid=2e0cb6f0795111eda4e70f80bb0a214f; sa-user-id=s%253A0-f2833463-5282-47df-52e9-687dc5e24bf6.4jaFjt5%252FB3JgtzPmncGVqQMZmSsfoeCKZzkBzOkXhqM; sa-user-id-v2=s%253A8oM0Y1KCR99S6Wh9xeJL9ox8tyE.DjyW22bC1mV4vAKdPD91GVAUYEC7cyNdJ4MD1%252FgrGtE; kampyleUserSession=1670762744813; kampyleUserSessionsCount=2; kampyleSessionPageCounter=1; _cs_id=9571a61d-4027-ad4d-87f1-460b3874ae32.1670762509.1.1670762744.1670762509.1.1704926509046; _cs_s=7.0.0.1670764544850; _px3=dee6b0caa1d3d74f1e5ecccf9e6846e4715a6d31ac73a830afc2eea78b8a7ab7:eSFaWcSOmgHMH0NCAyxXtRaR4U+yVYxt72InTD/xSjpwNxh1agGt4smtEw35YhmUoInYHy7LUSSFUQl2ZlD9wQ==:1000:oa76SAbZFSCWsu1jCvIR6JUTp3EEPVa3kOZndlj18VgPoZh06ObzeisBXdLbRGZ/fsDyjti8d0DIe0cvPGKv4RfAY5UH7gKZFcCKhNknemfK4PaF/m4Ca9GfKvpLxOUhoNw2j2ZUfxo5zidxIK3gnUDHjo2+xSD0b98UOptT+oT8JfOAtS6XiptsQLN5eX9RYfuuq4c19Yj95aa+eXdGfg==; _px=eSFaWcSOmgHMH0NCAyxXtRaR4U+yVYxt72InTD/xSjpwNxh1agGt4smtEw35YhmUoInYHy7LUSSFUQl2ZlD9wQ==:1000:ay+Cz4dUE5vuBFtfuIuC6pVDqMu4UlrGwx6Li34NlzDBn+PUw+DsLXZN3YplwI9oi15X/pQvHA6JP262l6P7drzPhMd2SajGzzV5pOjJ+x0l1Ft+WDTEUuxHHI9wRDQJr+HPrV3y7G1iIz8+GYXtpzNHpyZLymnPo/3jE/64JNOTz/4ym4BP+jIqouNFv+7Hy9vsq/zPRx7oc3oyH2LD/vdmwC2aFVo7MCz4u5t9bRz385b+3YkpIh7PdLHXDrtWnYNau2LldGbM6za3XCixZA==", "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36", "apollographql-client-name": "chegg-web"}
    res = requests.get(topic['url'], headers = header)
    soup = BeautifulSoup(res.content, 'html.parser')
    answer_div = soup.find('div', id='mobile-question-style')
    if answer_div is None:
        answer_div = soup.find('div', class_ = ['sc-crrsfI', 'hZASTv', 'sc-bYEvPH', 'cYjKgc'])
    result = {'data': [], 'status': True}
    if answer_div is not None:
        answers = answer_div.find_all(['div', 'p'])   
        if len(answers) == 0:
            result['data'].append({'id': f"{topic['topic_Id']}_1", 'topic_Id': f"{topic['topic_Id']}", 'content': answer_div.text, 'type': 'text'})
        answer_row = 1
        for page in range(0, len(answers)):
            topic_content = {'id': f"{topic['topic_Id']}_{answer_row}", 'topic_Id': f"{topic['topic_Id']}"}
            topic_content['content'] = f"{answers[page].text}"
            topic_content['type'] = "text"
            result['data'].append(topic_content)
            answer_row += 1
            img  = answers[page].find('img')
            if img is not None:
                topic_content = {'id': f"{topic['topic_Id']}_{answer_row}", 'topic_Id': f"{topic['topic_Id']}"}
                img_url = img.get('src')
                if "https:" not in img_url:
                    img_url = "https:" + img_url
                picture = requests.get(img_url)
                with open(f"./topic_pictures/{topic['topic_Id']}_{answer_row}.jpg", 'wb') as f:
                    f.write(picture.content)
                topic_content['content'] = f"{topic['topic_Id']}_{answer_row}.jpg"
                topic_content['type'] = "picture"
                result['data'].append(topic_content)
                answer_row += 1
    else:
        result['status'] = False
    return result


def ecAnswers(answers_html, topic_id):
    soup = BeautifulSoup(answers_html, 'html.parser')
    answer_div_arr = soup.find_all('div', class_ = ['sc-z3f5s1-1', 'gkwtCW'])
    answer_row = 1
    result = []
    for answer_div in answer_div_arr:
        img = answer_div.find('img')
        if img is not None:
            picture = requests.get(img.get('src'))
            with open(f"./answer_pictures/{topic_id}_{answer_row}.jpg", 'wb') as f:
                f.write(picture.content)
            topic_answer = {'id': f"{topic_id}_{answer_row}", 'topic_Id': f"{topic_id}", 'content': f"{topic_id}_{answer_row}.jpg", 'type': 'picture'}
            result.append(topic_answer)
            answer_row += 1
        else:
            div_arr = answer_div.find_all('div', class_ = ['public-DraftStyleDefault-block', 'fzJtOB'])
            for div in div_arr:
                topic_answer = {'id': f"{topic_id}_{answer_row}", 'topic_Id': f"{topic_id}", 'content': div.text, 'type': 'text'}
                result.append(topic_answer)
                answer_row += 1
            li_arr = answer_div.find_all('li', class_ = 'jlQIUk')
            for li in li_arr:
                topic_answer = {'id': f"{topic_id}_{answer_row}", 'topic_Id': f"{topic_id}", 'content': li.text, 'type': 'text'}
                result.append(topic_answer)
                answer_row += 1
    return result

def htmlAnswers(answers_html, topic_id):
    soup = BeautifulSoup(answers_html, 'html.parser')
    p_arr = soup.find_all(['p', 'pre'])
    answer_row = 1
    result = []
    for p in p_arr:
        img = p.find('img')
        topic_answer = {'id': f"{topic_id}_{answer_row}", 'topic_Id': f"{topic_id}"}
        if img is not None:
            img_url = img.get('src')
            if "https:" not in img_url:
                img_url = "https:" + img_url
            picture = requests.get(img_url)
            with open(f"./answer_pictures/{topic_id}_{answer_row}.jpg", 'wb') as f:
                f.write(picture.content)
            topic_answer['content'] = f"{topic_id}_{answer_row}.jpg"
            topic_answer['type'] = 'picture'
            result.append(topic_answer)
            answer_row += 1
        else:
            topic_answer['content'] = p.text
            topic_answer['type'] = 'text'
            result.append(topic_answer)
            answer_row += 1
    return result

def sqnaAnswers(answers_text, topic_id):
    answers_json = json.loads(answers_text)
    answer = answers_json['stepByStep']['steps'][0]['blocks'][0]['block']['editorContentState']['blocks'][0]['text']
    result = [{'id': f"{topic_id}_1", 'topic_Id': f"{topic_id}", 'content': answer, 'type': 'text'}]
    return result

def getAnswer(topic_id):
    header = {"cookie": "V=b2140bf89fc7eabc45c7ba72c972b7c9634a3025a45a1c.51511721; _pxvid=c87e8b83-4c3d-11ed-8278-55796c79786e; _ga=GA1.2.26429051.1665806374; _gcl_au=1.1.724149711.1665806374; _cs_c=0; optimizelyEndUserId=oeu1665806518098r0.32436547692540074; _tt_enable_cookie=1; _ttp=9489700a-f4c3-4b14-9b92-357cc4d39e5b; chegg_web_cbr_interstitial=b99; _gid=GA1.2.450147349.1668429968; C=0; O=0; forterToken=3436ffb3f0624a7381d6de3b29b6bb4a_1668430001371__UDF43_13ck; refresh_token=ext.a0.t00.v1.McLryPBMBgKhb57Fzw6YbjVI26ZDlqjd6YhCr55jhMO4u9eiFFLE7_9V00XjvZ4vdJ3eevXAu8mxKA24bqP2fnM; id_token=eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InRvbnkxMjM0NTY1NTVAZ21haWwuY29tIiwiaXNzIjoiaHViLmNoZWdnLmNvbSIsInN1YiI6IjhkMDkyMWJmLTUxMzItNDIyYS1hYmJhLTBkMTBhYWFlNDJiNiIsImF1ZCI6IkNIR0ciLCJpYXQiOjE2Njg0MzAwMzMsImV4cCI6MTY4Mzk4MjAzMywicmVwYWNrZXJfaWQiOiJhcHcifQ.CB4IGkHKnp2sENrcWU5eqRDwDYuICxPi1nIIa8YH643r1rErYCU0WCmeM694XiRRkiE8Ye8z4KFkYYW-VURkZ1M1jbdQ2pU9QD_sn4COIhoKFAq_LQlAK_2AxJpy7KAwsdjElZ5lGCtFRaX2_i-xeuj3eAALMXKbsTxCl78n58zvkYc5eyTUjRrPuXm9l2QwjQo9BI-5EHQ39YBwL5weSG-O1ItaT4zRwe2Nc0vAru640ms7_dvIxflb6kMG6qvIpWbuoJACn0t0JO0XYWBGtzS25_zL_iK9g9lxqSIa52Rln5QFVg6GUpudyAacppSiYa-2pQh80-qImDDLa_Tnfw; exp=C026A%7CA127D; expkey=649839316CBE9221315720CCA48652F1; U=5ffced0af7b4881fb277fcd00d6d8d12; gid=1; gidr=MA; _pubcid=61ca9ab2-d50d-420e-937c-823df0cb5d87; _iidt=h3i57FW+hmkMVxDw4TOcBTQpGZifqYpC/vRas6NZU1vQkgW0WzA/34TtmRxdZev3ubc/vbnZucup+aR9ZSLFcCdo7g==; _vid_t=/tV/AMT6agl/bWUp7SGqGvuNVrzGRO4vetYqD8Hs0NGHyKpZHY386uRmbBQibdijuUf/k2JKyykE3iAezT/Gmq23XQ==; DFID=web|rGEWTz86QqtJRvvkJ7Cu; country_code=TW; CVID=3e4d0937-096e-4221-8389-807a54b03702; CSID=1668509723349; user_geo_location=%7B%22country_iso_code%22%3A%22TW%22%2C%22country_name%22%3A%22Taiwan%22%2C%22region%22%3A%22TPE%22%2C%22region_full%22%3A%22Taipei+City%22%2C%22city_name%22%3A%22Taipei%22%2C%22locale%22%3A%7B%22localeCode%22%3A%5B%22zh-TW%22%5D%7D%7D; pxcts=036291b2-64d4-11ed-ac95-75776a696859; PHPSESSID=u869gc984e2ebirsda27qiv2fh; CSessionID=98c02ba1-1f9f-4e68-ba8a-77b2ad075988; SU=KHVHSZu3LQMhmnDgh1bR8ICkGfuAzkrhPAwf0NkKnMKQSMId1jsKrQvlb5tSi2AEkhIUGcUNayAXx-TutVH15fX29wzdlUg2khWaqSf_sV7smFda3kGJehLejvJ6PRbn; local_fallback_mcid=45510271206596629357107185496663449932; s_ecid=MCMID|45510271206596629357107185496663449932; IR_gbd=chegg.com; opt-user-profile=b2140bf89fc7eabc45c7ba72c972b7c9634a3025a45a1c.51511721%252C22027151712%253A22030841005%252C22285540405%253A22272140252%252C21582730015%253A21585790242%252C22228540454%253A22268720225; _rdt_uuid=1668510439777.a116d4a7-492a-44d9-99b4-bcf415529a5c; ab.storage.deviceId.b283d3f6-78a7-451c-8b93-d98cdb32f9f1=%7B%22g%22%3A%221e576bac-7999-e856-9e76-3ff3b6ce38a2%22%2C%22c%22%3A1667637761399%2C%22l%22%3A1668514232230%7D; ab.storage.userId.b283d3f6-78a7-451c-8b93-d98cdb32f9f1=%7B%22g%22%3A%228d0921bf-5132-422a-abba-0d10aaae42b6%22%2C%22c%22%3A1668430036867%2C%22l%22%3A1668514232230%7D; _cs_cvars=%7B%221%22%3A%5B%22Page%20Name%22%2C%22question%20page%22%5D%2C%223%22%3A%5B%22Page%20Type%22%2C%22pdp%22%5D%7D; OptanonConsent=isIABGlobal=false&datestamp=Tue+Nov+15+2022+21%3A03%3A19+GMT%2B0800+(%E5%8F%B0%E5%8C%97%E6%A8%99%E6%BA%96%E6%99%82%E9%96%93)&version=6.39.0&hosts=&consentId=13e0e439-c784-4f89-8593-99fc88284b53&interactionCount=1&landingPath=NotLandingPage&groups=snc%3A1%2Cfnc%3A1%2Cprf%3A1%2Ctrg%3A1&AwaitingReconsent=false&isGpcEnabled=0; _px3=05666d8005d69d116bbc039fcebdffe874acf4f7d90adfe124a394b44ae0fb33:s+qE2tcaUFVsHecCwpWHMHuCUG8pCt8EhEMEKdbvlnxCCD61oL2xJrNPNhEbEIosbVIpGP52CuVsrsr22JqHag==:1000:4eZQ13UXTRpg+vzicDGlx3Bua9o5AXph0tcX3p18FeWdAptKzbHhCpj6yZFi4xava7h/etZ5IvfdyRtYU9KTSfI3s6LnldsFBIgoqY10YiZYQFUYlKx3zYb94c9qp7zGp5YkYkaUi2nOEL4WWaeJy3vOtWOx0eBzM/YHs4FiVvl7vtPpJqNRIcbnhfIKZxIVkLWsjXC0SRmGFGsRRGXQWQ==; _px=s+qE2tcaUFVsHecCwpWHMHuCUG8pCt8EhEMEKdbvlnxCCD61oL2xJrNPNhEbEIosbVIpGP52CuVsrsr22JqHag==:1000:frrF5cfZnrPogHX/WtRLDnaCEnXpHpi25j0tb//7ZT5cUF+wrraKdrWCO5F4NTRj+k9k3FDgniXN7Ko/GP/nR1RAeQQZMbKskvzUs8CGu1QpklsTxyM7EBQAD3qacvUbCLrV2Xnd0HOy640RnvyF0jJ3rwjGl3AfRW6+4jbL7XMkeHlB24u7od81K4xIrQ5ecLES2T0lK1MyZWO+/sH+1b52jQsxbmsB3/Pw2z9yV0z3thQSIOy9Pt0RNxVoMKO9tMX6wknNSeuJ8Fw0E0voHQ==; IR_14422=1668517399999%7C0%7C1668517399999%7C%7C; _awl=2.1668517399.0.5-65aa31fcff8638a2f9281d863c2ef5db-6763652d617369612d6561737431-1; _uetsid=4f0b62f0641a11ed8b53df0d5fb1c55f; _uetvid=c78e58404c3d11ed9830edb1cd2755c1; _gat=1; ab.storage.sessionId.b283d3f6-78a7-451c-8b93-d98cdb32f9f1=%7B%22g%22%3A%2233517f05-2626-7c2d-32ec-524fbe97bf54%22%2C%22e%22%3A1668519200806%2C%22c%22%3A1668514232229%2C%22l%22%3A1668517400806%7D; _cs_id=5f374409-ef25-a45f-cd03-31651d5749f0.1665806519.6.1668517401.1668513691.1.1699970519655; _cs_s=14.0.0.1668519201412", "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36", "authorization": "Basic TnNZS3dJMGxMdVhBQWQwenFTMHFlak5UVXAwb1l1WDY6R09JZVdFRnVvNndRRFZ4Ug==", "apollographql-client-name": "chegg-web"}
    payload = {"operationName":"QnaPageAnswer","variables":{"id":None},"extensions":{"persistedQuery":{"version":1,"sha256Hash":"b51b81abb2515ec8e28b5b1a86efbff1d04eb39cdad257775432e411a3d3ad45"}}}
    payload['variables']['id'] = topic_id
    res = requests.post("https://gateway.chegg.com/one-graph/graphql", headers = header, json = payload)
    questionByLegacyId =  res.json()['data']['questionByLegacyId']
    result = ""
    if questionByLegacyId['htmlAnswers'] is not None:
        answers_html = questionByLegacyId['htmlAnswers'][0]["answerData"]['html']
        result = htmlAnswers(answers_html, topic_id)
    elif questionByLegacyId['ecAnswers'] is not None:
        answers_html = questionByLegacyId['ecAnswers'][0]['answerData']['steps'][0]['textHtml']
        result = ecAnswers(answers_html, topic_id)
    elif questionByLegacyId['sqnaAnswers'] is not None:
        answers_text = questionByLegacyId['sqnaAnswers']['answerData'][0]['body']['text']
        result  = sqnaAnswers(answers_text, topic_id)
    else:
        print(f"{topic_id} 無解答")

    return result
   

def getAll(topic):
    try:
        mysql = MySql()
        result = getTopics(topic)
        if result['status']:
            mysql.SaveTopic(topic)
            for item in result['data']:
                mysql.SaveTopicContent(item)
            time.sleep(5)
            data = getAnswer(topic['topic_Id'])
            for item in data:
                mysql.SaveTopicAnswer(item)
            time.sleep(5)
        mysql.DBClose()
    except:
        time.sleep(20)
        getAll(topic)


def test(topic):
    mysql = MySql()
    result = getTopics(topic)
    if result['status']:
        for item in result['data']:
            mysql.SaveTopicContent(item)
    else:
        print(topic['topic_Id'])
        mysql.DBClose()
        time.sleep(15)
        test(topic)


if __name__ == '__main__':
    query = "tree"
    for page in range(7, 10):
        print(f"第{page}頁")
        topics = getTopicsInfo(page, query)
        for topic_index in range(0, len(topics)):
            getAll(topics[topic_index])